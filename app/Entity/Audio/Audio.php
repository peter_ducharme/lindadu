<?php

namespace App\Entity\Audio;

use Illuminate\Database\Eloquent\Model;
use App\Entity\Setting\Setting;
use App\Entity\Category\Category;


class Audio extends Model
{
    protected $table = 'audio';




    public function getDisdateAttribute($value)
    {
        return date("F jS, Y",strtotime($this->attributes['date']));;
    }


}
