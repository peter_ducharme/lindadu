<?php 
namespace App\Entity\Category;
use Illuminate\Foundation\Http\FormRequest;
class CategoryRequest extends FormRequest
{

// Determine if the user is authorized to make this request.
    public function authorize()
    {
        return true;
    }

	public function rules()
	{
		return [
		'name' => 'required|min:3'
		];
	}
	
	public function messages()
    {
        return [
        'name.required' => 'A name is required.',
        'name.min:3' => 'Name must be three letters long.',
        ];
    }

}