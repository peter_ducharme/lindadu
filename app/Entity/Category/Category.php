<?php
namespace App\Entity\Category;

use Illuminate\Database\Eloquent\Model;
use App\Entity\Setting\Setting;

class Category extends Model
{
    protected $table = 'categorys';

    protected $fillable = ['name'];

    protected $casts = [
//      'theme' => 'json',
    ];


    public function getSlugAttribute($value) {
        return str_slug($this->name);
    }
/*
    public function getPublishdateAttribute($value)
    {
        return date("m/d/Y",strtotime($value));
    }

    public function setPublishdateAttribute($value)
    {
        $this->attributes['publishdate'] = date("Y-m-d",strtotime($value));
    }
*/
}
