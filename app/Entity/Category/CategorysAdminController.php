<?php    
namespace App\Entity\Category;

use Auth;
use Illuminate\HttpResponse;
use App\Http\Controllers\Controller;

use App\Entity\Category\CategoryRequest;
use App\Entity\Category\Category;

class CategorysAdminController extends Controller {

	public function __construct()
	{
// 		$this->middleware('auth');
	
	}

	
	public function create()
	{
		$category = new Category;
		return view('admin.category.create',compact('category'));
	}

	
	public function edit(Category $category)
	{
		return view('admin.category.edit', compact('category'));
	}


	public function show(Category $category)
	{
		return view('admin.category.show', compact('category'));
	}


	public function destroy(Category $category)
	{
		$category->delete();
		return redirect ('admin/categorys');
	}


	public function index()
	{
		$categorys = Category::orderBy('id', 'asc')->get();
		return view('admin.category.index',compact('categorys'));
	}

	public function store(CategoryRequest $request)
	{
		$category = new Category($request->all());
		$category->save();
		return redirect('admin/categorys')->with(['flash_message'=>'Your Category is saved.']);
	}


	public function update(CategoryRequest $request,Category $category)
	{
		$category->update($request->all());
		return redirect ('admin/categorys');
	}

}