<?php 
namespace App\Entity\Category;

use Auth;
use Illuminate\HttpResponse;
use App\Http\Controllers\Controller;

use App\Entity\Category\CategoryRequest;
use App\Entity\Category\Category;

class CategorysController extends Controller {

	public function __construct()
	{
// 		$this->middleware('auth');
	
	}


	public function show(Category $category)
	{
		return view('admin.categorys.show', compact('category'));
	}


	public function index()
	{
		$categorys = Category::orderBy('id', 'asc')->get();
		return view('admin.categorys.index',compact('categorys'));
	}

}