<?php
namespace App\Entity\Article;

use Illuminate\Database\Eloquent\Model;
use App\Entity\Setting\Setting;
use App\Entity\Category\Category;
use Illuminate\Support\Str;

class Article extends Model
{
    /**
     * @var string
     */
    protected $table = 'articles';

    /**
     * @var array
     */
    protected $fillable = ['title', 'teaser', 'body', 'category_id', 'date', 'picture'];


    /**
     * @var array
     */
    protected $casts = [
//      'theme' => 'json',
    ];

    /**
     * Pass along categories mapped n:n
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(){
        return $this->belongsTo(Category::class);
    }

    /**
     * Convert title string to a standard slug for URLs
     * @return string
     */
    public function getSlugAttribute() {
        return Str::slug($this->title);
    }


    /**
     * Grab the teaser without HTML. Primarily for meta. Done as a safety.
     * @param $value
     * @return string
     */
    public function getTeaserAttribute($value)
    {
        return strip_tags($value);
    }


    /**
     * Make sure the teaser is stored without HTML.
     * @param $value
     */
    public function setTeaserAttribute($value)
    {
        $this->attributes['teaser'] = strip_tags($value);
    }


/*
// DATE STUBBS

public function getPublishdateAttribute($value)
    {
        return date("m/d/Y",strtotime($value));
    }

    public function setPublishdateAttribute($value)
    {
        $this->attributes['publishdate'] = date("Y-m-d",strtotime($value));
    }
*/
}
