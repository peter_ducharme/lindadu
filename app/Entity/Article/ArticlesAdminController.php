<?php
namespace App\Entity\Article;

use App\Entity\Article\Article;
use Auth;
use Illuminate\HttpResponse;
use App\Http\Controllers\Controller;

use App\Entity\Article\ArticleRequest;
use App\Entity\Category\Category;
use Illuminate\Support\Facades\File;

class ArticlesAdminController extends Controller {

    /**
     * ArticlesAdminController constructor.
     */
    public function __construct()
	{
// 		$this->middleware('auth');

	}

    /**
     * @return View
     */
    public function create()
	{
        $cats = Category::pluck('name', 'id');
	    $article = new Article;
		return view('admin.article.create',compact('article','cats'));
	}

    /**
     * @param Article $article
     * @return View
     */
    public function edit(Article $article)
	{
        $cats = Category::pluck('name', 'id');
		return view('admin.article.edit', compact('article','cats'));
	}

    /**
     * @param Article $article
     * @return View
     */
    public function show(Article $article)
	{
		return view('admin.article.show', compact('article'));
	}

    /** Remove image file if it exists
     * @param Article $article
     * @return Redirector
     * @throws \Exception
     */
    public function destroy(Article $article)
	{
        $image_file_location = public_path('imgs').$article->picture;
	    if(File::exists($image_file_location)){
          File::delete($image_file_location);
         }

	    $article->delete();
		return redirect ('admin/articles');
	}

    /**
     * @return View
     */
    public function index()
	{
		$articles = Article::orderBy('date', 'desc')->get();
		return view('admin.article.index',compact('articles'));
	}

    /**
     * @param ArticleRequest $request
     * @return Redirector
     */
    public function store(ArticleRequest $request)
	{
	    $article = new Article($request->all());
        $article->picture = $this->storeImage($request);
	    $article->save();
		return redirect('admin/articles')->with(['flash_message'=>'Your Article is saved.']);
	}

    /**
     * Includes image file storage routine.
     * @param ArticleRequest $request
     * @param Article $article
     * @return Redirector
     */
    public function update(ArticleRequest $request, Article $article)
	{
        $requestData = $request->all();
	    if(isset($request->picture)){
            $requestData['picture'] = $this->storeImage($request);
        }
        $article->update($requestData);
		return redirect ('admin/articles');
	}

    /**
     * @param $request
     * @return string - serialized filename of stored image
     */
    private function storeImage($request){
        $photoName = time().'.'.$request->picture->getClientOriginalExtension();
        $request->picture->move(public_path('imgs'), $photoName);
        return $photoName;
    }

}
