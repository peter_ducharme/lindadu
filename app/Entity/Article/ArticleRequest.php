<?php
namespace App\Entity\Article;
use Illuminate\Foundation\Http\FormRequest;
class ArticleRequest extends FormRequest
{

// Determine if the user is authorized to make this request.
    public function authorize()
    {
        return true;
    }

    /**
     * Do not need to require an image on update "PATCH"
     * @return array
     */
    public function rules()
	{
		$rules = [
            'title' => 'required|min:3',
            'picture' => 'required'
        ];

	    if ($this->method() == 'PATCH') {
	    unset($rules['picture']);
        }

	    return $rules;
	}

	public function messages()
    {
        return [
        'title.required' => 'A title is required.',
        'title.min:3' => 'Title must be three letters long.',
        'picture.required' => 'A image is required for the article.',

        ];
    }

}
