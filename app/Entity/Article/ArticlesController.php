<?php
namespace App\Entity\Article;

use App\Entity\Article\Article;
use App\Entity\Audio\Audio;
use App\Entity\Category\Category;
use Auth;
use Illuminate\HttpResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use ResponseCache;

use App\Entity\Article\ArticleRequest;

class ArticlesController extends Controller {

    /**
     * ArticlesController constructor.
     */
    public function __construct()
	{
// 		$this->middleware('auth');

	}

    /**
     * Get all articles with this particualr category
     * @param null $id
     * @return \View
     */
    public function categories($id = null)
    {
        $articles = Article::where('category_id',$id)->orderBy('id','desc')->get();
        return view('cat_view',compact('articles'));
    }

    /**
     * Get all categories
     * @return \View
     */
    public function allcategories()
    {
        $cats = Category::orderBy('id', 'asc')->get();
        return view('cats',compact('cats'));
    }


    /**
     * Show single article in main section
     *  Include up to two additional different articles in same category
     * @param null $id
     * @return \View
     */
    public function show($id = null)
    {
        $article = Article::find($id);
        $also_articles_incategory = Article::where('category_id',$article->category->id)->where('id', '!=', $id)->limit(2)->get();
        return view('article_view',compact('article','also_articles_incategory'));
    }

    /**
     * @return \View
     */
    public function about()
    {
        $also_articles_incategory = Article::inRandomOrder()->limit(2)->get();
        return view('about',compact('also_articles_incategory'));
    }

    /**
     * @return \View
     */
    public function audio()
    {
        $audios = Audio::orderBy('date', 'desc')->get();
        $also_articles_incategory = Article::inRandomOrder()->limit(2)->get();
        return view('audio',compact('also_articles_incategory','audios'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cacheclear()
    {
        Artisan::call('view:clear');
        ResponseCache::clear();
        Artisan::call('cache:clear');
        Artisan::call('responsecache:clear');
        return redirect('/');
    }


    /**
     * @return \View
     */
    public function homepage()
	{
		$articles = Article::orderBy('id', 'asc')->where('teaser','!=','teaser tk tk tk tk tk')->get();
        $collection = collect($articles);
        $shuffled = $collection->shuffle();
        $lead = $shuffled->slice(0,1);
        $feats = $shuffled->slice(1,6);
        $adds =  $shuffled->slice(7,4);
		return view('hp',compact('lead','feats','adds'));
	}

}
