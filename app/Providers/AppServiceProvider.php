<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Entity\Category\Category;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once __DIR__ . '/../Http/Helpers/func_helper.php';
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $categories = Category::orderBy('name', 'asc')->get();
        view()->share('cats', $categories);
    }
}
