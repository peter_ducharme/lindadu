<?php

/**
 * Test for the error and resturn a CSS class when an error is present
 * @param $fld -- field name
 * @param null $errors
 * @return string
 */
function redError($fld, $errors = null){
    if($errors->has($fld)){
        return " border-red";
    }
}

/**
 * Minify all css files and return one large string made up of CSS rules
 * @param $paths -- paths to css files
 * @return mixed
 */
function minCss($paths){
    $resp = "";

    foreach($paths as $path){
        $resp .= file_get_contents($path);
    }
    return HTMLMin::css($resp);
}
?>
