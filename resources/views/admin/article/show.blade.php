@extends('admin.main')
@section('content')

    <div class="flex flex-wrap _">
        <div class="bg-white border rounded shadow w-full">
            <div class="border-b p-3"><h5 class="uppercase text-grey-dark">Showing Article</h5></div>
            <div class="p-5">

                    <h2>{{$article->name}}</h2>
            </div>
        </div>
    </div>
@endsection