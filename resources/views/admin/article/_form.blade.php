<?php
$inputfieldclass = "appearance-none block w-full bg-grey-lightest text-black border border-grey-lighter focus:border-grey  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white";
?>
<div class="flex flex-wrap -mx-3 mb-6">

<!-- text title -->
<div class="w-full  px-3 mb-6 md:mb-0 cl-title">
    <label class="block uppercase tracking-wide text-grey text-xs font-bold mb-2" for="for-title">Title</label>
    {!! Form::text('title',null,['class' => $inputfieldclass.redError('title',$errors), 'id' => 'for-title']) !!}
    @include('admin.errors._msg',['fld' => 'title'])
</div>

<!-- select simple category -->
<div class="w-full md:w-1/2 px-3 mb-6 md:mb-0 cl-category_id">
    <label class="block uppercase tracking-wide text-grey text-xs font-bold mb-2" for="for-category_id">Category</label>
    {!! Form::select('category_id',$cats,null,['class' => $inputfieldclass.redError('category_id',$errors), 'id' => 'for-category_id']) !!}
    @include('admin.errors._msg',['fld' => 'category_id'])
</div>

<!-- text date -->
    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0 cl-date">
        <label class="block uppercase tracking-wide text-grey text-xs font-bold mb-2" for="for-date">Date Publish</label>
        {!! Form::text('date',null,['class' => $inputfieldclass.redError('date',$errors), 'id' => 'for-date']) !!}
        @include('admin.errors._msg',['fld' => 'date'])
    </div>

    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0 cl-date mt-10">
        <label class="block uppercase tracking-wide text-grey text-xs font-bold mb-2" for="for-date">Feature Image</label>
       @if(isset($article->picture))

           <img class="" src="/imgs/{!!$article->picture!!}"/>
           @endif

        {!! Form::file('picture',null,['class' => $inputfieldclass.redError('date',$errors), 'id' => 'for-picture']) !!}
        @include('admin.errors._msg',['fld' => 'picture'])
    </div>



<!-- textarea teaser -->
<div class="w-full  px-3 mb-6 md:mb-0 cl-teaser mt-10">
    <label class="block uppercase tracking-wide text-grey text-xs font-bold mb-2" for="for-teaser">Teaser</label>
    {!! Form::textarea('teaser',null,['class' => $inputfieldclass.redError('teaser',$errors), 'id' => 'for-teaser']) !!}
    @include('admin.errors._msg',['fld' => 'teaser'])
</div>



    <!-- text body -->
<div class="w-full  px-3 mb-6 md:mb-0 cl-body">
    <label class="block uppercase tracking-wide text-grey text-xs font-bold mb-2" for="for-body">Body</label>
    {!! Form::textarea('body',null,['class' => $inputfieldclass.redError('body',$errors), 'id' => 'for-body']) !!}
    @include('admin.errors._msg',['fld' => 'body'])
</div>

</div>
{!! Form::submit($submitButtonText,['class' => 'bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded']) !!}
</div>

@section('specific_js')
<script>
    $(function(){
        $( "#for-date" ).datepicker({dateFormat: "yy-mm-dd"});

        var blockQuoteButton = function (context) {
                    var ui = $.summernote.ui;
                    var button = ui.button({
                        className: 'note-btn-blockquote',
                        contents: '<i class="fa fa-quote-right"></i>',
                        tooltip: 'Blockquote',
                        click: function () {
                            context.invoke('editor.formatBlock', 'blockquote')
                        }
                    });
                    return button.render();   // return button as jquery object
                }

                $('#for-body').summernote({
                    tabsize: 2,
                    height: 400,
                    toolbar: [
                        ['insert', ['picture', 'link', 'hr']],
                        ['style', ['bold', 'italic', 'blockquote', 'underline', 'clear']],
                        ['para', ['style', 'ul', 'ol', 'paragraph']],
                        ['misc',['codeview']]
                    ],
                    buttons: {
                        blockquote: blockQuoteButton
                    }
                });


                        $('#for-teaser').summernote({
                            tabsize: 2,
                            height: 80,
                            toolbar: [
                                ['style', ['bold', 'italic', 'blockquote', 'underline', 'clear']],
                                ['misc',['codeview']]
                            ]
                        });



});

</script>
@endsection
