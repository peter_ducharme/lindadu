@extends('admin.main')
@section('content')
    <div class="flex flex-wrap">
        <div class="bg-white border rounded shadow w-full">
            <div class="flex justify-between items-center border-b p-3"><h3 class="uppercase text-grey-darker pl-3">Articles</h3>
                <a class="bg-transparent hover:bg-blue text-blue-dark text-xs uppercase no-underline hover:text-white py-2 px-4 border border-blue hover:border-transparent rounded"
                   href="{{action('\App\Entity\Article\ArticlesAdminController@create')}}">Create</a>
            </div>
            <div class="p-5">
@if(count($articles)>0)
                <table class="w-full p-5 text-grey-darker">
                    <thead>
                        <tr>
                            <th class="p-2 text-left text-blue-darkest">Title</th>
                            <th class="p-2 text-left text-blue-darkest">Teaser</th>
                            <th class="p-2 text-left text-blue-darkest">Category</th>
                            <th class="p-2 text-left text-blue-darkest">Action</th>
                        </tr>
                    </thead>
                    <tbody>
@foreach($articles as $article)
                        <tr>
                            <td class="p-2">{{$article->title}}</td>
                            <td class="p-2">{{substr($article->teaser,0,34)}}</td>
                            <td class="p-2">{{$article->category->name}}</td>

                            <td class="p-2 w-1/6">
                                <form action="/admin/articles/{{$article->id}}" method="POST">@method('DELETE')@csrf
                                    <a href="{{action('\App\Entity\Article\ArticlesAdminController@edit', $article)}}"><i class="text-grey hover:text-grey-darker text-lg fas fa-edit"></i></a>
                                    <button><i class="text-grey hover:text-grey-darker text-lg far fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
@endforeach
                    </tbody>
                </table>
@endif
            </div>
        </div>
    </div>
@endsection
