@extends('admin.main')
@section('content')

    <div class="flex flex-wrap _">
        <div class="bg-white border rounded shadow w-full">
            <div class="border-b p-3"><h5 class="uppercase text-grey-dark">Creating Article</h5></div>
            <div class="p-5">

                    {!! Form::model($article,['url' => 'admin/articles','class' => "w-full ", 'files' => true]) !!}
                    @include('admin.article._form',['submitButtonText' => 'Create Article'])
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection
