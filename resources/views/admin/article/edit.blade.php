@extends('admin.main')
@section('content')

    <div class="flex flex-wrap _">
        <div class="bg-white border rounded shadow w-full">
            <div class="border-b p-3"><h5 class="uppercase text-grey-dark">Editing Article</h5></div>
            <div class="p-5">

                    {!! Form::model($article,['method' => 'PATCH','class' => "w-full", 'files' => true, 'action' => ['\App\Entity\Article\ArticlesAdminController@update',$article->id]]) !!}
                    @include('admin.article._form',['submitButtonText' => 'Update Article'])
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection
