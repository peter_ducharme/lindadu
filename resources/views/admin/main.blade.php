<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Manage</title>
        <meta name="description" content="description here" />
        <meta name="keywords" content="keywords,here" />

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
        <link href="/manage/css/tailwind.min.css" rel="stylesheet" />

        <script type="text/javascript" src="/manage/js/jquery-3.3.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="/manage/js/select2.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
    </head>
    <body class="bg-grey-lightest font-sans leading-normal tracking-normal">
        <nav id="header" class="bg-white fixed w-full z-10 pin-t shadow">
            <div class="w-full container mx-auto flex flex-wrap items-center mt-0 pt-3 pb-3 md:pb-0">
                <div class="w-1/2 pl-2 md:pl-0">
                    <a class="text-black text-base xl:text-xl no-underline hover:no-underline font-bold" href="/admin">
                    <i class="fas fa-cogs text-blue-dark pr-3"></i>
                    Manage Panel</a>
                </div>
                <div class="w-1/2 pr-0">
                    <div class="flex relative inline-block float-right">
                        <div class="relative text-sm">
                            <button id="userButton" class="flex items-center focus:outline-none mr-3">
                                <img class="w-12 h-12 rounded-full mr-3" src="{{'http://www.gravatar.com/avatar/' . md5(Auth::user()->email) . '?s=200'}}" alt="{{ Auth::user()->name }}" /> <span class="hidden md:inline-block">{{ Auth::user()->name }} </span>
                                <svg class="pl-2 h-2" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129"
                                     enable-background="new 0 0 129 129">
                                    <g><path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z" /></g>
                                </svg>
                            </button>
                            <div id="userMenu" class="bg-white rounded shadow-md mt-2 absolute mt-12 pin-t pin-r min-w-full overflow-auto z-30 invisible">
                                <ul class="list-reset">
                                    <li><a href="#" class="px-4 py-2 block text-black hover:bg-grey-light no-underline hover:no-underline">My account</a></li>
                                    <li><a href="#" class="px-4 py-2 block text-black hover:bg-grey-light no-underline hover:no-underline">Notifications</a></li>
                                    <li><hr class="border-t mx-2 border-grey-ligght" /></li>
                                    <li><a href="{{ url('/logout') }}" class="px-4 py-2 block text-black hover:bg-grey-light no-underline hover:no-underline">Logout</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="block lg:hidden pr-4">
                            <button id="nav-toggle" class="flex items-center px-3 py-2 border rounded text-grey border-grey-dark hover:text-black hover:border-teal appearance-none focus:outline-none">
                                <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <title>Menu</title>
                                    <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="w-full flex-grow lg:flex lg:items-center lg:w-auto hidden lg:block mt-2 lg:mt-0 bg-white z-20" id="nav-content">
                    <ul class="list-reset lg:flex flex-1 items-center px-4 md:px-0">
                        <li class="mr-6 my-2 md:my-0 {{ request()->is('admin') ? ' active' : '' }}">
                            <a href="/admin" class="block py-1 md:py-3 pl-1 align-middle text-grey-dark no-underline hover:text-black border-b-2 border-white hover:border-blue-dark">
                            <span class="pb-1 md:pb-0 text-sm">Home</span> </a>
                        </li>
                        <li class="mr-6 my-2 md:my-0 {{ request()->is('admin/articles*') ? ' active' : '' }}">
	<a href="/admin/articles" class="block py-1 md:py-3 pl-1 align-middle text-grey-dark no-underline hover:text-black border-b-2 border-white hover:border-blue-dark">
	<span class="pb-1 md:pb-0 text-sm">Article</span> </a>
</li>

<li class="mr-6 my-2 md:my-0 {{ request()->is('admin/categorys*') ? ' active' : '' }}">
	<a href="/admin/categorys" class="block py-1 md:py-3 pl-1 align-middle text-grey-dark no-underline hover:text-black border-b-2 border-white hover:border-blue-dark">
	<span class="pb-1 md:pb-0 text-sm">Category</span> </a>
</li>

                    </ul>


                </div>
            </div>
        </nav>
<div>&nbsp;</div>
        <!--Container-->
        <div class="container w-full mx-auto pt-20">
            <div class="w-full px-4 md:px-0 md:mt-8 mb-16 text-sm text-grey-darkest tracking-wide leading-normal">
              @yield('content')
            </div>
        </div>
        <!-- modal -->
        <div  class=" fixed z-50 pin overflow-auto bg-smoke flex hide">
            <div class=" fixed  max-w-md md:relative pin-b pin-x align-top m-auto justify-end md:justify-center p-8 bg-white md:rounded w-full md:h-auto md:shadow flex flex-col">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, dicta doloremque error expedita facere inventore minus nam natus odit optio perspiciatis placeat, provident quae repellat repudiandae rerum sapiente, voluptas. Error!</p>
            </div>
        </div>


@yield('specific_jslib')
@yield('specific_js')
    <script>
        /*Toggle dropdown list*/
        /*https://gist.github.com/slavapas/593e8e50cf4cc16ac972afcbad4f70c8*/

        var userMenuDiv = document.getElementById('userMenu');
        var userMenu = document.getElementById('userButton');

        var navMenuDiv = document.getElementById('nav-content');
        var navMenu = document.getElementById('nav-toggle');

        document.onclick = check;

        function check(e) {
            var target = (e && e.target) || (event && event.srcElement);

            //User Menu
            if (!checkParent(target, userMenuDiv)) {
                // click NOT on the menu
                if (checkParent(target, userMenu)) {
                    // click on the link
                    if (userMenuDiv.classList.contains('invisible')) {
                        userMenuDiv.classList.remove('invisible');
                    } else {
                        userMenuDiv.classList.add('invisible');
                    }
                } else {
                    // click both outside link and outside menu, hide menu
                    userMenuDiv.classList.add('invisible');
                }
            }

            //Nav Menu
            if (!checkParent(target, navMenuDiv)) {
                // click NOT on the menu
                if (checkParent(target, navMenu)) {
                    // click on the link
                    if (navMenuDiv.classList.contains('hidden')) {
                        navMenuDiv.classList.remove('hidden');
                    } else {
                        navMenuDiv.classList.add('hidden');
                    }
                } else {
                    // click both outside link and outside menu, hide menu
                    navMenuDiv.classList.add('hidden');
                }
            }
        }

        function checkParent(t, elm) {
            while (t.parentNode) {
                if (t == elm) {
                    return true;
                }
                t = t.parentNode;
            }
            return false;
        }



    var element = document.querySelector("trix-editor");

    document.addEventListener("trix-action-invoke", function(event) {
        if(event.actionName === "x-insertimage"){
            console.log("Insert Image");
            var rng = element.editor.getSelectedRange();
            element.editor.setSelectedRange(rng);
            element.editor.insertHTML("<strong>Hello</strong>")

        }
    })


    </script>

    </body>


</html>
