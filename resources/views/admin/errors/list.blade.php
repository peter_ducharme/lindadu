<div class="col-md-11">
@if($errors->any())
    <ul class="alert list-unstyled">
        @foreach($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    </ul>
@endif

</div>
