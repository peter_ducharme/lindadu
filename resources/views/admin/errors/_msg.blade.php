@if ($errors->has($fld))
<p class="text-red text-xs italic">{{$errors->first($fld)}}</p>
@endif
