@extends('admin.main')
@section('content')
    <div class="flex flex-wrap">
        <div class="bg-white border rounded shadow w-full">
            <div class="flex justify-between items-center border-b p-3"><h3 class="uppercase text-grey-darker pl-3">Categorys</h3>
                <a class="bg-transparent hover:bg-blue text-blue-dark text-xs uppercase no-underline hover:text-white py-2 px-4 border border-blue hover:border-transparent rounded"
                   href="{{action('\App\Entity\Category\CategorysAdminController@create')}}">Create</a>
            </div>
            <div class="p-5">
@if(count($categorys)>0)
                <table class="w-full p-5 text-grey-darker">
                    <thead>
                        <tr>
                            <th class="p-2 text-left text-blue-darkest">Name</th>
                            <th class="p-2 text-left text-blue-darkest">Action</th>
                        </tr>
                    </thead>
                    <tbody>
@foreach($categorys as $category)
                        <tr>
                            <td class="p-2">{{$category->name}}</td>

                            <td class="p-2 w-1/6">
                                <form action="/admin/categorys/{{$category->id}}" method="POST">@method('DELETE')@csrf
                                    <a href="{{action('\App\Entity\Category\CategorysAdminController@edit', $category)}}"><i class="text-grey hover:text-grey-darker text-lg fas fa-edit"></i></a>
                                    <button><i class="text-grey hover:text-grey-darker text-lg far fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
@endforeach
                    </tbody>
                </table>
@endif
            </div>
        </div>
    </div>
@endsection