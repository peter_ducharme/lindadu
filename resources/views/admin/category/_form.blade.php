<?php
$inputfieldclass = "appearance-none block w-full bg-grey-lightest text-black border border-grey-lighter focus:border-grey  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white";
?>
<div class="flex flex-wrap -mx-3 mb-6">

    <!-- name text -->
    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
        <label class="block uppercase tracking-wide text-grey text-xs font-bold mb-2" for="grid-first-name"> Name </label>
        {!! Form::text('name',null,['class' => $inputfieldclass.redError('name',$errors), 'id' => 'name']) !!}
        @include('admin.errors._msg',['fld' => 'name'])
    </div>


</div>
{!! Form::submit($submitButtonText,['class' => 'bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded']) !!}
</div>

@section('specific_js')
<script>
    $(function(){
});
</script>
@endsection