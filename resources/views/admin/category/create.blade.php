@extends('admin.main')
@section('content')

    <div class="flex flex-wrap _">
        <div class="bg-white border rounded shadow w-full">
            <div class="border-b p-3"><h5 class="uppercase text-grey-dark">Creating Category</h5></div>
            <div class="p-5">

                    {!! Form::model($category,['url' => 'admin/categorys','class' => "w-full max-w-md"]) !!}
                    @include('admin.category._form',['submitButtonText' => 'Create Category'])
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection