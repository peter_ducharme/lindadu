@extends('admin.main')
@section('content')

    <div class="flex flex-wrap _">
        <div class="bg-white border rounded shadow w-full">
            <div class="border-b p-3"><h5 class="uppercase text-grey-dark">Editing Category</h5></div>
            <div class="p-5">

                    {!! Form::model($category,['method' => 'PATCH','class' => "w-full max-w-md", 'action' => ['\App\Entity\Category\CategorysAdminController@update',$category->id]]) !!}
                    @include('admin.category._form',['submitButtonText' => 'Update Category'])
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection