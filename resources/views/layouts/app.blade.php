<!DOCTYPE html>
<html>
<head>
    <title>Linda DuCharme: {{$article->title ?? " Writer"}}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-title" content="{{$article->title ?? "Linda DuCharme: Writer"}}">
    <meta name="application-name" content="Linda DuCharme: Writer">
    <meta name="author" content="Linda DuCharme">
    <meta name="description" content="{!!$article->teaser ?? "The collected writings that first appeared in the Brattleboro Reformer."!!}">
    <meta name="msapplication-TileColor" content="#ff5656">
    <meta name="msapplication-TileImage" content="{{Request::root()}}/css/cropped-LD-favicon-01-1-180x180.png">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:description" content="{!!$article->teaser ?? "The collected writings that first appeared in the Brattleboro Reformer."!!}">
    <meta name="twitter:title" content="{{$article->title ?? "Linda DuCharme: Writer"}}">
    <meta name="type" content="Article">
    <meta name="vertical" content="Linda DuCharme: Writer">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta property="fb:app_id" content="966242223397117">
    <meta property="og:description" content="{!!$article->teaser ?? "The collected writings that first appeared in the Brattleboro Reformer."!!}">
    <meta property="og:site_name" content="Linda DuCharme: Writer">
    <meta property="og:title" content="{{$article->title ?? "Linda DuCharme: Writer"}}">
    <meta property="og:url" content="{{Request::fullUrl()}}">
    <meta property="og:type" content="{{Request::fullUrl()}}">



@if(isset($article->picture))
    <meta property="og:image" content="{{Request::root()}}/imgs/{!!$article->picture!!}">
    <meta  name="twitter:image"content="{{Request::root()}}/imgs/{!!$article->picture!!}">
        <meta property="og:image:width" content="1700" />
        <meta property="og:image:height" content="1116" />
@else
    <meta property="og:image" content="{{Request::root()}}/imgs/lducharem.w615.h350.2x.png">
        <meta  name="twitter:image"content="{{Request::root()}}/imgs/lducharem.w615.h350.2x.png">
        <meta property="og:image:width" content="600" />
        <meta property="og:image:height" content="315" />

    @endif

    <link rel="icon" href="{{Request::root()}}/css/cropped-LD-favicon-01-1-32x32.png" sizes="32x32" />
    <link rel="icon" href="{{Request::root()}}/css/cropped-LD-favicon-01-1-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="{{Request::root()}}/css/cropped-LD-favicon-01-1-180x180.png" />
    <meta name="msapplication-TileImage" content="{{Request::root()}}/css/cropped-LD-favicon-01-1-270x270.png" />
    <link rel="canonical" href="{{Request::fullUrl()}}" />



    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({  google: {    families: ["Tenor Sans:regular","Source Serif Pro:regular,700","Libre Franklin:regular,600","Ultra:regular"]  }});</script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
    <script src="https://unpkg.com/wavesurfer.js"></script>
    <style>
        <?php
echo minCss([public_path('/css/normalize.css'),public_path('/css/webflow.css'),public_path('/css/linda-ducharme.webflow.css')]);
?>
    </style>
</head>
<body>
<div data-collapse="medium" data-animation="default" data-duration="400" class="navigation w-nav">
    <div class="navigation-container"><a href="/" class="logo w-inline-block"><h1 class="logo_hdr">Linda DuCharme</h1></a>
        <nav role="navigation" class="nav-menu w-nav-menu">
            <a href="/about" class="navigation-link w-inline-block">
                <div class="navigation-link-text">About</div>
                <div class="navigation-hover" style="opacity: 0; height: 0px;"></div>
            </a>

            <a href="/audio" class="navigation-link w-inline-block">
                <div class="navigation-link-text">Vermont Public Radio</div>
                <div class="navigation-hover" style="opacity: 0; height: 0px;"></div>
            </a>

            <a href="/category" class="navigation-link w-inline-block">
                <div class="navigation-link-text">Category</div>
                <div class="navigation-hover" style="opacity: 0; height: 0px;"></div>
            </a>
        </nav>
        <div class="menu-button w-nav-button">
            <div class="icon-2 w-icon-nav-menu"></div>
        </div>
    </div>
    <div class="w-nav-overlay" data-wf-ignore=""></div>
</div>
@yield('content')
<div class="footer">
    <div class="container-footer">
        <div class="container">
            <div class="top-section">
                <div class="footer-small-text">© 2018 Linda DuCharme™, all rights reserved</div>
            </div>
        </div>
        <div class="separator-footer">
            <div class="separator-color footer-line"></div>
        </div>
        <div class="container">
            <div class="bottom">
                <div class="bottom-1">
                    <div data-delay="0" class="dropdown w-dropdown">
                        <div class="dropdown-toggle w-dropdown-toggle">
                            <div class="icon w-icon-dropdown-toggle"></div>
                            <div>Category</div>
                        </div>
                        <nav class="dropdown-list w-dropdown-list">
                            <div class="w-dyn-list">
                                <div class="w-dyn-items">
                                    <div class="dropdown-link w-dyn-item"><a href="/audio" class="dropdown-link-text">Vermont Public Radio</a></div>
@foreach($cats as $cat)
                                    <div class="dropdown-link w-dyn-item"><a href="/category/{{$cat->id}}/{{$cat->slug}}" class="dropdown-link-text">{!! $cat->name !!}</a></div>
@endforeach
                                </div>
                            </div>
                        </nav>
                    </div>
                    <div class="footer-categories">
                        <div class="footer-title-text">Categories</div>
                        <div class="footer-links-container cc-categories">
                            <div class="w-dyn-list">
                                <div class="collection-list w-dyn-items">
                                    <div class="footer-collection-item w-dyn-item"><a href="/audio" class="link-footer">Vermont Public Radio</a></div>
@foreach($cats as $cat)
                                <div class="footer-collection-item w-dyn-item"><a href="/category/{{$cat->id}}/{{$cat->slug}}" class="link-footer">{!! $cat->name !!}</a></div>
@endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="https://d1tdp7z6w94jbb.cloudfront.net/js/jquery-3.3.1.min.js" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="/js/webflow.js" type="text/javascript"></script>
<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>
