@extends('layouts.app')
@section('content')

    <div class="title-section">
        <div  class="container cc-center">
            <div class="text-container">
                <a href="#" class="section-title-text w-inline-block">
                    <div class="post-category-text">About</div>
                </a>
                <h1 class="h1">Linda DuCharme</h1>

            </div>
        </div>
    </div>
    <div class="post-content">
        <div class="container cc-center  biowrap">

            <img src="/imgs/mom.png"  sizes="100vw" alt="Linda DuCharme">

            <p>
                Linda DuCharme is a writer who lives in Vermont. She
                has been the editor of newspapers in Brattleboro, Vermont, and in
                Milford, Connecticut, where she also did news photography and wrote a
                humorous column about family life.
            </p>

        </div>
    </div>
    <div class="separator">
        <div class="separator-container">
            <div class="line-color"></div>
        </div>
    </div>
    @include('latest_posts')
@endsection


