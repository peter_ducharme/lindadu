<div class="latest-posts">
    <div class="container">
        <div class="section-title-text">Latest POSTS</div>
        <div class="posts-wrapper">
            <div class="posts-collection-list-wrapper w-dyn-list">
                <div class="posts-collection-list w-dyn-items w-row">
                    @foreach($also_articles_incategory as $oarticle)

                        <div class="_2-collection-item w-dyn-item w-col w-col-6">
                            <a href="/articles/{{$oarticle->id}}/{{$oarticle->slug}}" class="posts-image w-inline-block">
                                <img src="/imgs/{!!$oarticle->picture!!}"  alt=""></a>
                            <div class="post-info-text">
                                <a href="/category/{{$oarticle->category->id}}/{{$oarticle->category->slug}}" class="category-link">{!! $oarticle->category->name !!}</a><br>
                                <a href="/articles/{{$oarticle->id}}/{{$oarticle->slug}}" class="post-title w-inline-block">
                                    <h2 class="h3">{{$oarticle->title}}</h2>
                                </a>
                                <div class="post-author-text cc-small-thumbnail">
                                    <div class="post-author cc-top-margin">By Linda DuCharme</div>
                                </div>
                                <div class="rich-text w-richtext" style="margin-top:12px;">
                                    {!! $oarticle->teaser !!}
                                </div>

                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
