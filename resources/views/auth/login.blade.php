@extends('layouts.app')

@section('content')
<div class="container">





    <div class="about-blog">
        <div class="container cc-center">
            <div class="text-container">
                <div class="section-title-big">Login</div>
                <div class="w-form">
                    <form id="email-form" method="POST" action="{{ route('login') }}">
                        @csrf

                        <label for="email">Email Address</label>
                        <input id="email" type="email" class="w-input form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <div class="w-form-fail">
                                <div>{{ $errors->first('email') }}</div>
                            </div>
                        @endif

                        <label for="password">Password</label>
                        <input id="password" type="password" class="w-input form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" required autofocus>

                        @if ($errors->has('password'))
                            <div class="w-form-fail">
                                <div>{{ $errors->first('password') }}</div>
                            </div>
                        @endif

                        <label for="remember">Remember me</label>
                        <input class="w-input form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>




                        <input type="submit" value="Submit" data-wait="Please wait..." class="w-button">
                    </form>


                </div>
            </div>
        </div>
    </div>




@endsection
