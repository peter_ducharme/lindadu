@extends('layouts.app')
@section('content')
<div class="top-post">
    <div class="container">
        <div class="posts-wrapper cc-top-post">
            <div class="w-dyn-list">
                <div class="w-dyn-items">
                    <div class="top-post-item w-dyn-item">
                        <a href="/articles/{{$lead[0]->id}}/{{$lead[0]->slug}}" class="top-post-image w-inline-block"><img src="/imgs/{!! $lead[0]->picture !!}" alt="" sizes="100vw" class="post-image"></a>
                        <div class="top-post-text">
                            <a href="/category/{{$lead[0]->category->id}}/{{$lead[0]->category->slug}}" class="category-link">{!! $lead[0]->category->name !!}</a><br>
                            <a href="/articles/{{$lead[0]->id}}/{{$lead[0]->slug}}" class="top-post-link-block w-inline-block">
                                <h2 class="h2">{{$lead[0]->title}}</h2></a>
                            <div class="post-short-text">
                                {!! $lead[0]->teaser !!}
                            </div>
                            <div class="post-author-text">
                                <div class="post-author cc-top-margin">By Linda DuCharme</div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="_3-posts">
    <div class="container">
        <div class="posts-wrapper">
            <div class="posts-collection-list-wrapper w-dyn-list">
                <div class="posts-collection-list w-dyn-items w-row">
                    @foreach($feats as $feat)
                        <div class="_3-collection-item w-dyn-item w-col w-col-4">
                            <a href="/articles/{{$feat->id}}/{{$feat->slug}}" class="posts-image w-inline-block"><img src="/imgs/{!! $feat->picture !!}" alt="" sizes="100vw"></a>
                            <div class="post-info-text"><a href="/category/{{$feat->category->id}}/{{$feat->category->slug}}" class="category-link">{!! $feat->category->name !!}</a><br><a href="/articles/{{$feat->id}}/{{$feat->slug}}" class="post-title w-inline-block">
                                    <h3 class="h3">{!! $feat->title !!}</h3></a>
                                <div class="post-author-text cc-small-thumbnail">
                                    <div class="post-author cc-top-margin">By</div><span class="post-author">Linda DuCharme</span></div>
                            </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="separator">
    <div class="separator-container">
        <div class="line-color"></div>
    </div>
</div>
<div class="featured-posts">
    <div class="container">
        <div class="section-title-text">featured POSTS</div>
        <div class="posts-wrapper">
            <div class="posts-collection-list-wrapper w-dyn-list">
                <div class="posts-collection-list w-dyn-items w-row">
@foreach($adds as $add)
                        <div class="_2-collection-item w-dyn-item w-col w-col-6">
                            <a href="/articles/{{$add->id}}/{{$add->slug}}" class="posts-image w-inline-block"><img src="/imgs/{!! $add->picture !!}" alt="" sizes="100vw"></a>
                            <div class="post-info-text"><a href="/category/{{$add->category->id}}/{{$add->category->slug}}" class="category-link">{!! $add->category->name !!}</a><br><a href="/articles/{{$add->id}}/{{$add->slug}}" class="post-title w-inline-block">
                                    <h2 class="h3">{!! $add->title !!}</h2></a>
                                <div class="post-author-text cc-small-thumbnail">
                                    <div class="post-author cc-top-margin">By</div><span class="post-author">Linda DuCharme</span></div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
