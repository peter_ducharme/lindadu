@extends('pblc.main')

@section('content')

		<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('/postimage/edit_{{$setting->hpimage}}')">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<div class="site-heading">
					<h1>Bob DuCharme’s blog</h1>
					<!--
					<hr class="small">
					<span class="subheading">Things &amp; Stuff</span>-->
				</div>
			</div>
		</div>
	</div>
</header>


<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
<?php $post = $posts[0]; ?>
	<div class="post-preview">
		<a href="/blog/{{$post->slug}}">
			<h2 class="post-title">
				{{$post->title}}
			</h2>
			@if($post->subtitle !='')<h3 class="post-subtitle">{{$post->subtitle}}</h3>@endif
		</a>

		<p>{!! $post->bodyprev !!}
			<a href="/blog/{{$post->slug}}"><strong>..more</strong></a>
		</p>

		<p class="post-meta">Published {{$post->publishdatefrm}}</p>
	</div>
	<hr>

	@foreach($posts as $k=>$post)
@if($k>0)
		<div class="post-preview">
			<a href="/blog/{{$post->slug}}">
				<h2 class="post-title">
					{{$post->title}}
				</h2>
				@if($post->subtitle !='')<h3 class="post-subtitle">{{$post->subtitle}}</h3>@endif
			</a>
			<p class="post-meta">Published {{$post->publishdatefrm}}</p>
		</div>
		<hr>
@endif
		@endforeach
	<!-- Pager -->
	<ul class="pager">
		<li class="next">
			<a href="/posts">All Posts →</a>
		</li>
	</ul>
</div>







		</div>
	</div>

</div>
@endsection



@section('specific_jslib')
<script src="/js/jquery.js"></script>
@endsection

@section('specific_js')
<script>
$(function(){


})
</script>
@endsection
<!--  -->