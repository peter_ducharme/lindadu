@extends('layouts.app')
@section('content')


    <div class="title-section cc-detail">
        <div class="container">

            @foreach($cats as $cat)
                <h2 class="h2 cc-title-section">
                <a href="/category/{{$cat->id}}/{{$cat->slug}}">
                    {!! $cat->name !!}
                </a>
                </h2>
            @endforeach


        </div>
    </div>



@endsection


