@extends('layouts.app')
@section('content')


    <div class="title-section cc-detail">
        <div class="container">
            <h2 class="h2 cc-title-section">{!! $articles[0]->category->name !!}</h2>
            <div class="posts-wrapper">
                <div class="posts-collection-list-wrapper w-dyn-list">
                    <div class="posts-collection-list w-dyn-items w-row">

                        @foreach($articles as $article)
                            <div class="_3-collection-item w-dyn-item w-col w-col-4">
                                <a href="/articles/{{$article->id}}/{{$article->slug}}" class="posts-image w-inline-block"><img src="/imgs/{!! $article->picture !!}" alt="" sizes="100vw"></a>
                                <div class="post-info-text"><a href="/articles/{{$article->id}}/{{$article->slug}}" class="post-title w-inline-block">
                                        <h3 class="h3">{!! $article->title !!}</h3></a>
                                    <div class="post-author-text cc-small-thumbnail">
                                        <div class="post-author cc-top-margin">By</div><span class="post-author">Linda DuCharme</span></div>
                                    <div class="post-short-text" style="font-size: 1em;margin-top:12px;">
                                        {!! $article->teaser !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>



@endsection


