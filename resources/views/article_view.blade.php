@extends('layouts.app')
@section('content')
<?php
$pubDate =  date('Y-m-d', strtotime('2019-04-27 + '.$article->id.' days'));
?>

    <script type="application/ld+json">
{ "@context": "https://schema.org",
 "@type": "Article",
 "headline": "{{$article->title}}",
 "image": "{!! URL::to('/') !!}/imgs/{!!$article->picture!!}",
 "author": "Linda DuCharme",
 "editor": "Linda DuCharme",
 "genre": "{{$article->category->name}}",
 "url": "{!! URL::to('/') !!}",
   "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "{!! URL::to('/') !!}/articles/{{$article->id}}/{{$article->slug}}"
  },
  "publisher": {
    "@type": "Organization",
    "name": "Brattleboro Reformer",
     "logo": {
      "@type": "ImageObject",
      "url": "{!! URL::to('/') !!}/css/brattleboro-reformer.png"
    }
  },
 "datePublished": "{!! $pubDate !!}",
 "dateCreated": "{!! $pubDate !!}",
 "dateModified": "{!! $pubDate !!}",
 "description": "{!! $article->teaser !!}"

 }
</script>

    <div class="title-section">
        <div  class="container cc-center">
            <div class="text-container">
                <a href="/category/{{$article->category->id}}/{{$article->category->slug}}" class="section-title-text w-inline-block">
                    <div class="post-category-text">{{$article->category->name}}</div>
                </a>
                <h1 class="h1">{{$article->title}}</h1>
                <div class="post-author-text cc-center">
                    <div class="post-author">BY Linda DuCharme</div>
                </div>
            </div>
        </div>
    </div>
    <div class="post-image">
        <div class="container cc-post-image"><img src="/imgs/{!!$article->picture!!}"  sizes="100vw" alt=""></div>
    </div>
    <div class="post-content">
        <div class="post-content-wrapper">
            <div class="rich-text w-richtext">
                {!! $article->body !!}
            </div>
        </div>
    </div>
    <div class="separator">
        <div class="separator-container">
            <div class="line-color"></div>
        </div>
    </div>
@include('latest_posts')

@endsection


