<?php


$cach_time = 3600 * 24; //home page for one day
Route::get('/','\App\Entity\Article\ArticlesController@homepage')->middleware('cacheResponse:'.$cach_time);
Route::get('/articles/{id}/{slug}','\App\Entity\Article\ArticlesController@show');
Route::get('/category/{id}/{slug}','\App\Entity\Article\ArticlesController@categories');
Route::get('/category/','\App\Entity\Article\ArticlesController@allcategories');
Route::get('/about','\App\Entity\Article\ArticlesController@about');
Route::get('/audio','\App\Entity\Article\ArticlesController@audio');
Route::get('/clear','\App\Entity\Article\ArticlesController@cacheclear');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout','\App\Http\Controllers\Auth\LoginController@logout');

Route::group(['prefix' => 'admin','middleware' => ['auth','doNotCacheResponse']], function()
{
        Route::get('/', function () {return view('admin.index');});
        Route::resource('articles','\App\Entity\Article\ArticlesAdminController');
        Route::resource('categorys','\App\Entity\Category\CategorysAdminController');
});
